Address:
<address itemprop="address">
3D Soccer Simulation Team (MRL3D), <br/>
Mechatronics Research Laboratories (MRL),<br />
SYNTECH Center, Qazvin Azad University, Iran.<br />
Postal Code: 34199-15195 <br/>
</address>
<br />
Reache me any time by email at <span style="background-color:#eee; padding:0;">aref dot moqadam at gmail dot com</span>.<br />
Also by <span style="background-color:#eee; padding:0;">a dot moqadam at mrl-spl dot ir</span>.<br />

You can also find me on:
<ul>
  <li><a target="_blank" href="http://github.com/ArefMq"><span style="display: none;">Aref Moqadam Mehr's </span>github</a></li>
  <li><a target="_blank" href="http://ir.linkedin.com/in/moqadam"><span style="display: none;">Aref Moqadam Mehr's </span>Linked-in</a></li>
  <li><a target="_blank" href="https://qiau.academia.edu/ArefMoqadam"><span style="display: none;">Aref Moqadam Mehr's </span>Academia.edu</a></li>
  <li><a target="_blank" href="https://twitter.com/aref_mq"><span style="display: none;">Aref Moqadam Mehr's </span>Twitter</a></li>
  <li><a target="_blank" href="https://plus.google.com/+ArefMoqadam"><span style="display: none;">Aref Moqadam Mehr's </span>Google+</a></li>
  <li><a target="_blank" href="https://www.youtube.com/c/ArefMoqadam"><span style="display: none;">Aref Moqadam Mehr's </span>Youtube</a></li>
</ul>
