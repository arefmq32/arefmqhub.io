<ul>
<li>Machine Learning approaches for Computer Vision and Image Processing such as Convolutional Neural Networks</li>
<li>Image Understanding and Scene Recognition</li>
<li>3D Image Interpretation from Single Image through Machine Learning Methods</li>
<li>3D Image Reconstruction through Photogrammetry Methods</li>
<li>Robotic and Artificial Intelligent</li>
</ul>
