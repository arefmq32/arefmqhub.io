<p>I am almost graduate student from <a itemprop="affiliation" href="http://qiau.ac.ir">Qazvin Azad University</a>
(I will receive my bachelor in June). I studied Computer Engineering (Software Major).

I was former team leader of <a href="http://www.mrl-spl.ir">NAO bipedal laboratory</a> at <a href="http://www.mrl.ir">Mechatronics Research Laboratories (MRL)</a>. 
And before that, I was working in a startup group named <a href="http://to3d.net">NAB</a> under support of <a itemprop="worksFor" href="http://syntechcenter.ir">Software 
Research Center (Currently known as SYNTECH CENTER)</a> in Qazvin. In NAB we were working on 3D model reconstruction of 2D simple images (photos and videos), and we were developing tools using photogrammetry method offered publicly. 
Currently I am trying to find new academic career and I am looking forward to work in a research lab.


Beside my professional life, I am really interested in doing new things and having new experiences. This enthusiasm includes trying new foods, going in new places and doing new activities.


<br />
<a href="http://arefmq.blogspot.com" style="float:right">What I am doing now.</a>
<br />
</p>
