<!DOCTYPE html>

<!-- Designed by My friend, Novin Shahroudi -->
<html lang="en-US">
	<head>
	<meta charset="UTF-8">
    <meta name="author" content="عارف مقدم مهر" />
    <meta name="description" content="من دانشجوی رشته مهندسی نرم افزار در دانشگاه آزاد قزویم هستم. همچنین مشغول به کار در SRC بخش پروژه X می باشم. در کنار کار در SRC من سرپرست تیم روبات های انسان نمای نائو در مرکز تحقیقات MRL نیز هستم." />
    <meta name="keywords" content="عارف مقدم مهر, رباتیک, نائو, برنامه نویسی, AI, هوش مصنوعی, مهندسی نرم افزار, سایت شخصی" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>عارف مقدم مهر</title>

		<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon">
		<link rel="icon" href="../favicon.ico" type="image/x-icon">
		
		<script type="application/ld+json">
        {
          "@context" : "http://schema.org",
          "@type" : "Person",
          "name" : "عارف مقدم مهر",
          "url" : "http://mrl-spl.ir/~moqadam/fa/",
          "sameAs" : [
            "https://plus.google.com/+ArefMoqadam",
            "https://twitter.com/aref_mq",
            "http://github.com/ArefMq",
            "http://ir.linkedin.com/in/moqadam",
            "https://qiau.academia.edu/ArefMoqadam"
          ]
        }
        </script>
		
		<style>
		    @font-face {
                font-family: nastealiq;
                src: url(fa/IranNastaliq.ttf);
            }
            
		    @font-face {
                font-family: diplomat;
                src: url(fa/Far_Diplomat.ttf);
            }
		
			body {
			    direction: rtl;
				margin-left: 10%;
				margin-right: 10%;
			}

			p {
				text-align: justify;
			}

			a {
				text-decoration: none;
                color: gray;
                font-weight: bold;
			}

            a:visited {
                color: darkgray;
            }

			a:hover {
				color: #CC0033;
			}

            h1 {
                margin: 20px 0;
            }
            
			h2, h3 {
				font-family: diplomat;
			}

			hr {
				border: 0;
				height: 1px;
				background: #333;
				background-image: -webkit-linear-gradient(left, #333, #333, #ccc); 
				background-image:    -moz-linear-gradient(left, #333, #333, #ccc); 
				background-image:     -ms-linear-gradient(left, #333, #333, #ccc); 
				background-image:      -o-linear-gradient(left, #333, #333, #ccc); 
				margin-top: -15px;
				margin-bottom: 20px;
			}

			hr.symmetry {
				border: 0;
				height: 1px;
				background: #333;
				background-image: -webkit-linear-gradient(left, #ccc, #333, #ccc); 
				background-image:    -moz-linear-gradient(left, #ccc, #333, #ccc); 
				background-image:     -ms-linear-gradient(left, #ccc, #333, #ccc); 
				background-image:      -o-linear-gradient(left, #ccc, #333, #ccc); 
				margin: 0;
			}

			#cover {
				text-align: center;
			}
			#cover_links {
				list-style-type: none;
				text-align: center;
			}

			#cover_links ul {
				padding: 0;
				margin: 0 auto;
				width: 55%;
			}

			#cover_links ul li {
				display: inline;
			}

			#cover_links ul li:after {
				content: "|"; 
				padding: 0 .5em;
			}

			#cover_links ul li:last-child:after {
				content: ''; 
			}

			#footer	{
				margin-bottom: 30px;
				margin-top: 75px;
				direction: ltr;
			}

			#copyright {
				float: left;
				width: 40%;
				line-height: 1;
				margin-top: 7px;
				font-size: small;
			}

			#fastLink {
				float: left;
				width: 20%;
				text-align: center;
			}

			#modificationTime {
                font-size: small;
                text-align: right;
                float: right;
                width: 40%;
			}

			#ValidationButtons {
				font-size: small;
			}

			#portrait {
				display: block;
				position: relative;
				float: right;
				margin: 25px 0px 20px 30px;
				width: 200px;
				height: 200px;
				border-radius: 150px;
				-webkit-border-radius: 150px;
				-moz-border-radius: 150px;
				/*background: url(me.jpg) no-repeat;*/
				box-shadow: 0 0 8px rgba(0, 0, 0, .8);
				-webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .8);
				-moz-box-shadow: 0 0 8px rgba(0, 0, 0, .8);
			}		

			.cse .gsc-control-cse, .gsc-control-cse {
				padding: 0px;
			}
			
			#publications {
				clear: both;
			}

            #cover_title {
                font-size: 2em;
                font-family: nastealiq;
            }
			
			#iconic {
                width: 100%;
                position: relative;
                float: right;
                display: block;
			}
			
			#mission {
                margin: 80px 25px;
                display: block;
                font-size: 1.6em;
                font-family: nastealiq;
			}
			
			#media_text {
			    direction: ltr;
			}
			
			#about_text {
			    font-family: diplomat;
			}
			
			#contact_text {
			    font-family: diplomat;
			}
			
			@media screen and (max-width: 500px) {
				body {
					margin-left: 5%;
					margin-right: 5%;
				}
				
				#cover_links ul {
					padding: 0;
					margin: 0 auto;
					width: 95%;
					font-size: 1.3em;
					line-height: 1.8em;
				}
				
				#portrait {
					float: none;
					margin: 0 auto;
					margin-top: 30px;
				}
			}
		</style>
	</head>

	<body itemscope itemtype="http://schema.org/Person">
    <!-- Header -->
		<div id="cover">
			<div id="cover_title">
				<h1 itemprop="name">عارف مقدم مهر</h1>
			</div>

      <!-- Menu -->
			<div id="cover_links">
				<ul>
					<li><a href="#about">در باره من</a></li>
					<!--li><a href="courses">Courses</a></li-->
					<li><a href="#publications">مقالات</a></li>
					<!--li><a href="../projects">پروژه ها</a></li-->
					<li><a href="http://arefmq.blogspot.com">وبلاگ</a></li>
					<li><a href="poems">دل نوشته ها</a></li>
					<li><a href="links">لینک ها</a></li>
					<li><a target="_blank" href="fa/full-cv">رزومه</a></li>
					<li><a target="_blank" href="full-cv">(انگلیسی) رزومه</a></li>
					<li><a href="#contact">تماس</a></li>
					<li><a href="http://mrl-spl.ir/~moqadam">English</a></li>
				</ul>
			</div>
			
			<?php $lang='fa'; require_once("langBox.php"); ?>
		</div>

    <!-- Logo -->
        <div id="iconic">
    		<img src="images/me.jpg" id="portrait" itemprop="image" />
    		<div id="mission">
    		    <span>دنیا را باید رنگی ببینیم،</span><br />
    		    <span>چرا که می توانیم!</span>
    		</div>
        </div>

    <!-- Contents START -->
		<div id="about">
			<h2>در باره من</h2>
			<hr/>
			<div id="about_text" itemprop="description">
				<?php require_once("fa/about.htm"); ?>
			</div>
		</div>

		<div id="publications">
			<h2>مقالات</h2>
		<hr/>
			<div id="media_text">
				<?php require_once("publication.htm"); ?>
			</div>
		</div>
		
		<div id="contact">
			<h2>تماس با من</h2>
		<hr/>
			<div id="contact_text">
				آدرس:
				<address itemprop="address">
				مرکز تحقیقات نرم افزار (SRC.Systems),<br />
				دانشگاه آزاد اسلامی، واحد قزوین.<br />
				</address>
				<br />
				پست الکترونیکی:<br /> <span style="background-color:#eee; padding:0; font-family: 'Tahoma';">a dot moqadammehr at mrl-spl dot ir</span><br />
				<span style="background-color:#eee; padding:0; font-family: 'Tahoma';">aref at src dot systems</span><br /><br />
				می توانید من را در شبکه های اجتماعی زیر نیز بیابید:
				<ul style="font-family: 'Tahoma';">
				  <li><a target="_blank" href="https://plus.google.com/+ArefMoqadam">Google+</a></li>
				  <li><a target="_blank" href="http://github.com/ArefMq">github</a></li>
				  <li><a target="_blank" href="http://ir.linkedin.com/in/moqadam">Linked-in</a></li>
          		  <li><a target="_blank" href="https://qiau.academia.edu/ArefMoqadam">Academia.edu</a></li>
				</ul>
			</div>
		</div>
    <!-- Contents END -->

    <!-- Footer -->
		<div id="footer">
			<hr class="symmetry" />
			<div id="copyright">
				<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0; vertical-align:bottom;" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a>&nbsp;This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>. This includes all images, documents, texts and any other medium that I have used on my website.
			</div>
			
			<div id="fastLink">
				<a href="#top">&#x2b06;top</a>
 			    <br />
			    <a style="font-size:x-small;" href="http://mrl-spl.ir/~moqadam">Original Website</a>
			</div>
			
			<div id="modificationTime">
			  &copy; 2016 - Designed <a target="_blank" href="http://novinshahroudi.ir" style="color: #333;">Novin Shahroudi</a>, Revised by <a href="mrl-spl.ir/~moqadam" style="color: #333;">Aref Moqadam</a>!<br />
				Last Update on 
				<?php

				$index_page = 'index.php';
				if (file_exists($index_page)) {
						echo date ("F d, Y.", filemtime($index_page));
				}
				else
				{
					echo "some time in past, which I can't remember...";
				}
				?>

				<div id="ValidationButtons">
					<a href="http://validator.w3.org/check?uri=<?php echo(($_SERVER['HTTPS'] ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]); ?>">VALID HTML!</a>&nbsp;&&nbsp;
					<a href="http://jigsaw.w3.org/css-validator/validator?uri=<?php echo(($_SERVER['HTTPS'] ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]); ?>">VALID CSS!</a>&nbsp;&nbsp;
          <!-- Find a workaround for blocking nature of the following script -->
          <script type="text/javascript" src="validation.js"></script>
				</div>
			</div>
		</div>

    <!-- Place all other js here -->
    <!-- Google Font -->
		<script type="text/javascript">
			WebFontConfig = {
				google: { families: [ 'Titillium+Web::latin' ] }
			};
			(function() {
				var wf = document.createElement('script');
				wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
				  '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
				wf.type = 'text/javascript';
				wf.async = 'true';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(wf, s);
			})(); 
		</script>

	</body>
</html>
