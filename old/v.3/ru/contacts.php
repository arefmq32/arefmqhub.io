Address:
<address itemprop="address">
Software Research Center (SRC.Systems),<br />
Qazvin Azad University, Iran.<br />
</address>
<br />
I can be reached by email at <span style="background-color:#eee; padding:0;">a dot moqadammehr at mrl-spl dot ir</span>.<br />
Also by <span style="background-color:#eee; padding:0;">aref at src dot systems</span>.<br />
You can also find me on:
<ul>
  <li><a target="_blank" href="https://plus.google.com/+ArefMoqadam">Google+</a></li>
  <li><a target="_blank" href="http://github.com/ArefMq">github</a></li>
  <li><a target="_blank" href="http://ir.linkedin.com/in/moqadam">Linked-in</a></li>
  <li><a target="_blank" href="https://qiau.academia.edu/ArefMoqadam">Academia.edu</a></li>
</ul>
