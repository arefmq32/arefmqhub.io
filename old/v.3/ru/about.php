<p>Currently, I am studying Computer Engineering at <a itemprop="affiliation" 
href="http://qiau.ac.ir">Qazvin Azad University</a>. I am interested in 
Computer and Cognition Science and it's related field including 

<b>Computer Vision</b>, 
<b>Machine Learning</b>, 
<b>Localization and Mapping</b> and 
<b>Natural Language Processing (NLP)</b>. 

I am currently working at <a itemprop="worksFor" href="http://src.systems">Software 
Research Center (SRC.Systems)</a> in Qazvin. In SRC I am working on Green-SRC 
project which is focused on making buildings more intelligent so that they can 
communicate with humans and save the energy without dissatisfaction of the 
individuals.

In addition to my position in SRC, I am team leader of
<a href="http://www.aldebaran.com/en/humanoid-robot/nao-robot">NAO</a> 
<a href="http://www.mrl-spl.ir">bipedal laboratory</a> at 
<a href="http://www.mrl.ir">Mechatronics Research Laboratories (MRL)</a>. 


<br />
<a href="http://arefmq.blogspot.com" style="float:right">What I am doing now.</a>
<br />
</p>
