Adresse:
<address itemprop="address">
Software Research Center (SRC.Systems),<br />
Qazvin Azad Universität, Iran.<br />
</address>
<br />
Ich bin bei <span style="background-color:#eee; padding:0;">a dot moqadammehr at mrl-spl dot ir</span> erhältlich.<br />
Und bei <span style="background-color:#eee; padding:0;">aref at src dot systems</span>.<br />
Du kannst finden mich hier:
<ul>
  <li><a target="_blank" href="https://plus.google.com/+ArefMoqadam">Google+</a></li>
  <li><a target="_blank" href="http://github.com/ArefMq">github</a></li>
  <li><a target="_blank" href="http://ir.linkedin.com/in/moqadam">Linked-in</a></li>
  <li><a target="_blank" href="https://qiau.academia.edu/ArefMoqadam">Academia.edu</a></li>
</ul>
