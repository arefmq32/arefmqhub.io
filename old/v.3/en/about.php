<p>Currently, I am studying Computer Engineering at <a itemprop="affiliation" 
href="http://qiau.ac.ir">Qazvin Azad University</a>. I am interested in 
Computer and Cognition Science and it's related field including 

<b>Computer Vision</b>, 
<b>Machine Learning</b>, 
<b>Image Understanding</b> and 
<b>Computational Neuroscience</b>. 

I am currently working as team leader in
<a href="http://www.aldebaran.com/en/humanoid-robot/nao-robot">NAO</a> 
<a href="http://www.mrl-spl.ir">bipedal laboratory</a> at 
<a href="http://www.mrl.ir">Mechatronics Research Laboratories (MRL)</a>. 

Formerly, I was in a startup group named <a href="to3d.net">NAB</a> under support of <a itemprop="worksFor" href="http://src.systems">Software 
Research Center (SRC.Systems)</a> in Qazvin. In NAB we were working on 3D model reconstruction of 2D simple images (photos and videos), and we were developing tools using photogrammetry method offered publicly.

<br />
<a href="http://arefmq.blogspot.com" style="float:right">What I am doing now.</a>
<br />
</p>
