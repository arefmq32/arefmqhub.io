<p>Soy Aref, y yo vengo de Irán. Yo soy estudiante en Ciencias de la Computación. 
Estoy interesado en Visión artificial (Computer Vision) y inteligencia artificial. 
Corrientemente, soy estudiante del <a itemprop="affiliation" href="http://qiau.ac.ir">Qazvin Azad Universidad</a>. 

Yo soy Investigación a <a itemprop="worksFor" href="http://src.systems">Software Research Center (SRC.Systems)</a> a Qazvin.
En adición, yo soy capitan del 
<a href="http://www.aldebaran.com/en/humanoid-robot/nao-robot">NAO</a> 
<a href="http://www.mrl-spl.ir">bípedo laboratorio</a> a 
<a href="http://www.mrl.ir">Mechatronics Research Laboratories (MRL)</a>. 


<br />
<a href="http://arefmq.blogspot.com" style="float:right">qué estoy haciendo!</a>
<br />
</p>
