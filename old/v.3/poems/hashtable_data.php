<?php

$hashTable = array(

"3070a" => "3070a7ee67e0461aa4678951be934593dbccd410",
"197b4" => "197b4de6f415a1b001734b83b34b752baed1fdb4",
"60350" => "60350d381c11d23d5316b4a593d2542010c0ca72",
"4914a" => "4914a75a7572920a34bceece8e8e9496bda92330",
"b2282" => "b22827bc2264efcf7b92e27a74b3165532cd2d72",
"80983" => "8098375eb8da650504771e38a3e46536cf9ec3ed",
"47837" => "47837854a345528de656fb5cb460d142a6bbd062",
"0f7c5" => "0f7c5cd018749c6d53a328c13de7955a0544be7a",
"28701" => "287017dc83acb4e635899c3b7d357390e07ad844",
"95b43" => "95b43342b19e4cb404f695747ccea645868c4b52",
"b42a6" => "b42a6d93d796915222f6ffb2ffdd6137d93c1cdb",
"8d22e" => "8d22e87a55275bd89616850e20741cbfd4ee822a",
"68c5f" => "68c5f75d96a3e0ef04e368ccf8158bbc37967c2c",
"19959" => "1995912f0d230ac5a67618016f6fe54de9432d6b",
"9a22c" => "9a22c85394b4cbe462ed3b1a41c917498b551e35",
"76a6b" => "76a6b895a96ed4c4b9ac1ec714a017dd9056b2f8",
"0fca5" => "0fca524c59af0d4413067ade761419ab1e51c09f",
"2d399" => "2d399ede14c6fdbc9147bed5e8eb859b3630a953",
"90298" => "90298cf8cdcb1009fc5771c5a26155eeff751a50",
"f626b" => "f626bfe8a6716f0e60b0f3989b67f5ad2fa9fda2",
"47000" => "47000d841e0bf5c528435fc36806ad95cdb5d82b",
"b1c1d" => "b1c1d8736f20db3fb6c1c66bb1455ed43909f0d8",
"e6986" => "e69867ca7d5a7b0ab60a2a61e7b791c106f7bf64",
"74ab7" => "74ab78d80572d4f3ab034ec86c2065b748ca286e",
"2a831" => "2a831829d6fb62f565d1e908a0049ef9b32bf9e7",
"9f2fe" => "9f2feb0f1ef425b292f2f94bc8482494df430413",
"f6546" => "f6546f98cf7a2a38988414d2bf8d8b9a3f717bd4",
"fb16a" => "fb16a80bbb309e45571fd954ddce3995c02f99d2",
"41042" => "41042a6efbacdc0e0f9f9b090fbad8ff5a051e78",
"f38f4" => "f38f4e53faa2512bea285805c5ee4c9a2502d1d8",
"5116e" => "5116e40694ac48f654cb7b6816177e0e717237c6",
"d67cc" => "d67cca5aaed6ead2e1c1c6b6e1d20a3d14c9622f",
"32c8b" => "32c8bbff09c356265a96fb8385cfa141c9d92f76",
"e5e02" => "e5e0213249cd5bd8fb9d09bb50854072d3dfa7db",
"0da19" => "0da19b540f7c3526f24acba988b03df43b3c5cf3",
"e0661" => "e066133fdba5e5077ee034d757dc6dfcebd12979",
"3a85d" => "3a85dcbc961528b64077963f8497d86c96295d95",
"6e618" => "6e618a9fa998a1e4f11c92acc36ffc5ed100ad39",
"da388" => "da38860cb875cb64092e402d80d9fe29c4865b18",
"28909" => "28909cfb89b135ea1bd6846e24e24a7f3215f6bb",
"e6283" => "e6283da3e6889c973ba181becee89aac4dad9792",
"98057" => "980578c262f7bbccbd6cf9696ff451c34321d2c0",
);

$lasties = array(
	"8d22e" => array("19959", "9a22c", "e7d8e"),
	"68c5f" => array("19959", "9a22c", "e7d8e"),
	"b42a6" => array("af631", "57a80")
);

$connections = array(
	//"3070a" => array("b1c1d", "9f2fe"),
	"197b4" => array("e6986", "74ab7", "2a831", "9f2fe", "f6546", "fb16a", "41042", "f38f4", "6e618", "3a85d", "da388"),
	"60350" => array("e6986", "74ab7", "2a831", "9f2fe", "f6546", "fb16a", "41042", "f38f4", "6e618", "3a85d", "da388"),
	"4914a" => array("e6986", "d67cc"),
	"b2282" => array("32c8b", "0da19"),
	"80983" => array("e6986", "9f2fe"),
	"47837" => array("e6986", "9f2fe", "e0661", "6e618", "3a85d", "da388"),
	"0f7c5" => array("5116e", "e5e02"),
	"28701" => array("5116e", "e5e02"),
	"95b43" => array("e6986"),
	"b42a6" => array("e6986"),
	"8d22e" => array("e6986", "3a85d", "6e618", "da388"),
	"68c5f" => array("e6986", "3a85d", "6e618", "da388"),
	"76a6b" => array("e6986", "28909", "e6283"),
	"0fca5" => array("e6986", "28909", "e6283"),
	"2d399" => array("e6986", "9f2fe", "98057"),
	"90298" => array("9f2fe", "e6986", "74ab7", "2a831", "fb16a", "e0661"),
	"f626b" => array("e6986"),
	"47000" => array("e6986")
);

?>